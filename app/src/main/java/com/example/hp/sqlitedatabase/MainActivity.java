package com.example.hp.sqlitedatabase;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import database.DBHandler;
import database.Personne;

public class MainActivity extends AppCompatActivity {
     DBHandler dbHandler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbHandler=new DBHandler(getApplicationContext());
    }


    public void valider(View view){
        EditText nom=(EditText)findViewById(R.id.nom);
        EditText address=(EditText)findViewById(R.id.address);

        Personne personne=new Personne(1,nom.getText().toString(),address.getText().toString());
        nom.setText("");
        address.setText("");
        int count=dbHandler.AddPersonne(personne).getPersonneCount();
        Toast.makeText(getApplicationContext(),String.valueOf(count),Toast.LENGTH_SHORT).show();
    }
}
