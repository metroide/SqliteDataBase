package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hp on 14/02/2017.
 */

public class DBHandler extends SQLiteOpenHelper{


    // application context
    private Context context;

    // database version
    private static final int DATABASE_VERSION = 1;

    //DataBase name
    private static final String DATABASE_NAME = "germes";

    // name table name
    private static final String TABLE_PERSONNE = "personne";

    // colonne de la table personne

    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "nom";
    private static final String KEY_ADRESS = "adress";


    public DBHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_TABLE="CREATE TABLE "+TABLE_PERSONNE+ " ("
                + KEY_ID + " INTEGER PRIMARY KEY, "
                + KEY_NAME + " TEXT, "
                +KEY_ADRESS + " TEXT " + ")";
        db.execSQL(CREATE_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        /*
            - on supprime les tables existantes
            - il faudra onc specifier les pages sur lequels on a effectuer des modifications
         */
        db.execSQL(" DROP TABLE IF EXIST " + TABLE_PERSONNE);

        onCreate(db);

    }


    //ajoueter uen personne

    public DBHandler AddPersonne(Personne personne){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_NAME,personne.getNom());
        values.put(KEY_ADRESS,personne.getAddress());

        // insertion d'une ligne en bd

        db.insert(TABLE_PERSONNE,null,values);
        db.close();

        return this;
    }

    // lecture des donnee en base de donnees

    public  Personne ReadPersonne(int id){
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.query(TABLE_PERSONNE,new String[]{KEY_ID,KEY_NAME,KEY_ADRESS},KEY_ID+"=?",
                new String[]{String.valueOf(id)},null,null,null,null);

        if (cursor!=null)
            cursor.moveToFirst();
        Personne personne=new Personne(Integer.parseInt(cursor.getString(0)),cursor.getString(1),cursor.getString(2));


        return personne;
    }


    //recupere tous les elements en base de donnee

    public List<Personne> getAllPersonne(){
        List<Personne> personnes=new ArrayList<>();
        // requte de selection de toutes les personnes
        String selectQuery="SELECT * FROM "+ TABLE_PERSONNE;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(selectQuery,null);

        // parcours de la liste des personnes

        if (cursor.moveToFirst()){
            do {
                Personne personne=new Personne();
                personne.setId(Integer.parseInt(cursor.getString(0)));
                personne.setNom(cursor.getString(1));
                personne.setAddress(cursor.getString(2));
                // ajouter le contact a la liste
                personnes.add(personne);

            }while (cursor.moveToNext());
        }

        return personnes;
    }


    //compter le nombre d'element d'une table

    public int getPersonneCount(){
        String selectQuery="SELECT * FROM "+ TABLE_PERSONNE;
        SQLiteDatabase db=this.getReadableDatabase();
        Cursor cursor=db.rawQuery(selectQuery,null);
        int count=cursor.getCount();
        cursor.close();
        return count;
    }


    // mise a jour des donnee

    public int updatePersonne(Personne personne){
        SQLiteDatabase db=this.getWritableDatabase();
        ContentValues values=new ContentValues();
        values.put(KEY_NAME,personne.getNom());
        values.put(KEY_ADRESS,personne.getAddress());

        // mise a jour de la table

        return db.update(TABLE_PERSONNE,values,KEY_ID + " =?",new String[]{String.valueOf(personne.getId())});
    }


    // suppresion d'un element en base donnee

    public void deletePersonne(Personne personne){
        SQLiteDatabase db=this.getWritableDatabase();
        db.delete(TABLE_PERSONNE,KEY_ID + " =?",new String[]{String.valueOf(personne.getId())});
        db.close();
    }



}
