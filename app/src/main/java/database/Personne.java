package database;

/**
 * Created by hp on 14/02/2017.
 */

public class Personne {
    private Integer id;
    private String nom;
    private String address;

    public Personne(){

    }

    public Personne(Integer id, String nom, String address) {
        this.id = id;
        this.nom = nom;
        this.address = address;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
